<?php
/**
 * Created by yd
 * User: xiaojun.lan
 * Date: 2018/5/7
 * Time: 10:32
 */

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
// 配置文件目录
define('CONF_PATH', __DIR__ . '/../config/');

// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';


