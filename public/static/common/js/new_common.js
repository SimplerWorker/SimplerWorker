;

/**
 * Created by xiaojun.lan on 2018/1/4.
 */

$('body').on("touchstart",function(){
    //空函数即可});
});

/**
 * 检查值是否为空
 * @param myValue 一个值
 */
function checkEmptyValue(myValue) {
    return myValue == '' || myValue == null || myValue == 'undefined';
}

/**
 * 检查对象赋值是否为空
 * @param myObject 一个input对象
 */
function checkEmptyByObject(myObject) {
    var myValue = myObject.val();
    if(myValue == '' || myValue == null || myValue == 'undefined'){
        myObject.css('border','1px solid red');
        return true;
    } else {
        return false;
    }
}

/**
 * 动态检测用户输入时,input输入的是不是数字,包括小数
 * @param myObj input对象
 */
function checkInputIsNumber(myObj) {
    myObj.bind('input propertychange',function (e) {
        e.preventDefault();
        var myValue = myObj.val();
        if(!myValue || ! /^\d+(\.\d+)?$/.test(myValue) ){
            myObj.css('border','1px solid red');
        }else {
            myObj.css('border','');
        }

    });
}

/**
 * 动态检测输入的是不是手机号码
 * @param myObj input对象
 */
function checkInputIsMobileNumber(myObj) {
    myObj.bind('input propertychange',function (e) {
        e.preventDefault();
        var myValue = myObj.val();
        if(!myValue || !/^1[3|4|5|7|8|9][0-9]\d{4,8}$/.test(myValue) ){
            myObj.css('border','1px solid red');
        }else {
            myObj.css('border','');
        }

    });

}

/**
 * 验证字符串是否是整数,不包括小数
 * @param myValue 一个值
 * @returns {boolean}
 */
function checkNumber(myValue) {
    var reg = /^[0-9]+$/;
    return reg.test(myValue);

}

/**
 * 验证字符串是否是数字,包括小数
 * @param myValue 一个值
 * @returns {boolean} 是:true 否:false
 */
function checkIsFloat(myValue) {
    var reg = /^\d+(\.\d+)?$/;
    return reg.test(myValue);

}

/**
 * 根据数组下标删除元素
 * @param index
 * @returns {boolean}
 */
Array.prototype.arrayDelByIndex=function(index){
    if(isNaN(index)||index>=this.length){
        return false;
    }
    for(var i=0,n=0;i<this.length;i++){
        if(this[i]!=this[index]){
            this[n++]=this[i];
        }
    }
    this.length-=1;
};

/**
 * 通用modal提示框
 * @param title 标题
 * @param content 提示内容
 */
function showCommonModal(title, content) {
    $('#commonModal').modal({keyboard: true});
    $("#commonModalLabel").html(title);
    $("#commonModalBody").empty();
    $("#commonModalSubmitCkeck").empty();
    $("#commonModalBody").append('<div style="font-size: 2rem;">'+content+'</div>');
    var htmlSubBtn = '<button type="button" id="deleteImageBtn" class="btn btn-primary">删除</button>' +
        '<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>';
    $("#commonModalSubmitCkeck").append(htmlSubBtn);
}

/**
 * 把字符串中所有匹配字符替换掉
 * @param str 字符串
 * @param beReplaced 字符串中要被匹配的子字符串
 * @param replaceWith 要替换为replaceWith
 */
function replaceAll(str,beReplaced,replaceWith) {
    if(!checkEmptyValue(str)){
        var reg = new RegExp(beReplaced,"g");//g,表示全部替换。
        return str.replace(reg,replaceWith);
    }else {
        return str;
    }

}

/**
 * 获取当前域名
 * @returns {string}
 */
function loginUrl() {
    var protocol = window.location.protocol;//协议
    var hostname = window.location.hostname;//主机名

    var pathName = window.location.pathname.substring(1);//去掉字符串第一个字符
    var DirName = pathName == '' ? '' : pathName.substring(0, pathName.indexOf('/'));
    var reg = new RegExp("%20", "g");
    var dir = DirName.replace(reg, " ");
    if(dir.length > 0){
               dir = '/' + dir;
    }
    return protocol + '//' + hostname + dir + '/login.html';
}


/**
 * 检测是不是ip4地址
 * @param strIP
 * @returns {boolean} true:正确的IP地址  false:错误的IP地址
 */
function isIP(strIP) {
    if (checkEmptyValue(strIP)) return false;
    var re = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/g //匹配IP地址的正则表达式
    if (re.test(strIP)) {
        if (RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256) return true;
    }
    return false;
}

/**
 * 检测是不是Mac地址,例如:00:24:21:19:BD:E4
 * @param mac
 * @returns {boolean} true:正确的Mac地址  false:不正确的Mac地址
 */
function isMac(mac) {
    var reg_mac=/[A-F\d]{2}:[A-F\d]{2}:[A-F\d]{2}:[A-F\d]{2}:[A-F\d]{2}:[A-F\d]{2}/;
    return reg_mac.test(mac);
}


/**
 * 检测是不是ip4地址/端口类型
 * @param strIP 192.168.1.1/24 ,斜杠后面的数值不能大于24
 * @returns {boolean}
 */
function isIPPort(strIP) {
    if (checkEmptyValue(strIP)) return false;
    var re = /^(\d+)\.(\d+)\.(\d+)\.([0-9]{1,3})\/([0-9]{1,3})$/g //匹配IP地址的正则表达式
    if (re.test(strIP)) {
        if (RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256 && RegExp.$5 < 25) return true;
    }
    return false;
}

/**
 * 检测是不是ip4地址段
 * @param strIP  192.168.1.1-192.168.1.2  ip段到ip段
 * @returns {boolean}
 */
function isIpToIp(strIP) {
    if (checkEmptyValue(strIP)) return false;
    var re = /^(\d+)\.(\d+)\.(\d+)\.([0-9]{1,3})\-(\d+)\.(\d+)\.(\d+)\.([0-9]{1,3})$/g //匹配IP地址的正则表达式
    if (re.test(strIP)) {
        if (RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256 && RegExp.$5 < 256&& RegExp.$6 < 256&& RegExp.$7 < 256&& RegExp.$8 < 256) return true;
    }
    return false;
}



