<?php
/**
 * Created by yd
 * User: xiaojun.lan
 * Date: 2018/5/15
 * Time: 9:27
 */


namespace app\worker\controller;

use \GatewayWorker\Lib\Gateway;
use think\Db;
use think\Controller;
use think\Config;
use app\api\controller\Rbac;
use app\api\controller\Utils;

class Index extends Controller
{

    protected $isLogin; // 是否登录

    public function _initialize()
    {
        $rbac = new Rbac();
        $this->isLogin = $rbac->checkAdminLoginStatus();

    }

    /**
     * 初始化webSocket用户
     * 只有登录之后才能初始化webSocket用户
     * 这里只有admin后台，并没有user前台，所以不用考虑admin的id和user的id重复的情况
     *
     * 如果有admin管理员用户，也有user用户，两者id可能重复，需要对uid进行分类：
     *      例如 admin的id一般不超过1000个，admin的uid就是admin的id；，即uid=id
     *          user的uid可以为id+1000，即uid=id+1000
     */
    public function initUser()
    {
        $client_id = input('client_id');
        if (!$client_id) {
            return Utils::arrayFormat([], 80052, 'client_id 不能为空');
        }

        if (!$this->isLogin) {
            return Utils::arrayFormat([], 2001, '用户未登录');
        }

        $uid = Utils::getAdminId();
        if ($uid) {
            $registerIpPort = Config::get('register_ip_port');
            Gateway::$registerAddress = $registerIpPort;
            Gateway::bindUid($client_id, $uid);
            return Utils::arrayFormat([]);
        }
        return Utils::arrayFormat([], 80053, '服务器获取管理员id失败');

    }

    /**
     * 客户端post发送信息，服务器webSocket回复例子
     * @return string
     */
    public function doSomeThing()
    {

        $uid = Utils::getAdminId();
        $json = json_encode(array('msg' => 'test ','uid'=>$uid));
        if (!is_array($json)) {
            if (!IS_WIN) {
                $registerIpPort = Config::get('register_ip_port');
            } else {
                $registerIpPort = '127.0.0.1:1236';
            }
            Gateway::$registerAddress = $registerIpPort;
            Gateway::sendToUid($uid, $json); // 常驻内存的子进程
            Gateway::sendToAll($json);
        }
        return $json;
    }

    public function doSomeThing1()
    {

        // workerman不允许这么干，就算是在thinkPHP中进行pcntl_fork()也不行（会影响workerman的运行）
        /*$pid = pcntl_fork();
        if ($pid == -1) {
            dump('fork失败');
        } elseif ($pid) {
            pcntl_wait($status);
        } else {
            dump('子进程');
        }*/

    }

}

