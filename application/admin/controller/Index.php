<?php

namespace app\admin\controller;
use app\api\controller\Rbac;
use app\api\controller\Utils;
use think\Controller;
use think\Db;
use think\Session;

/**
 * Created by SimplerWorker
 * User: xiaojun.lan
 * Date: 2018/5/7
 * Time: 13:36
 */
class Index extends Controller
{

    public function _initialize()
    {
    }

    ///////登录相关 start///////////////
    /**
     * 用户登录
     * @return array|mixed
     */
    public function login(){
        $name = input('name');

        if($name){
            $rbac = new Rbac();
            $loginInfo = $rbac->adminLogin();
            return $loginInfo;
        }else{
            return $this->fetch('login');
        }
    }
    /**
     * 退出登录
     */
    public function logout(){
        $rbac = new Rbac();
        return $rbac->adminLogout();
    }
    ///////登录相关 end  ///////////////

    ///////角色操作相关 start///////////////
    public function editRole(){
        $type = input('type');
        if($type){
            $rbac = new Rbac();
            switch ($type){
                case 'add':
                    return $rbac->editRole();
                    break;
                case 'list':
                    return $rbac->showRoleList();
                    break;
                case 'one':
                    return $rbac->findRole();
                    break;
                case 'update':
                    return $rbac->editRole();
                    break;
                case 'delete':
                    return $rbac->deleteRole();
                    break;
            }
        }


        return $this->fetch('editRole');

    }
    ///////角色操作相关 end  ///////////////

    ///////管理员-角色操作相关 start///////////////
    public function editAdminRole(){

        $type = input('type');
        if($type){
            $rbac = new Rbac();
            switch ($type){
                case 'add':
                    return $rbac->addAdminRole();
                    break;
                case 'list':
                    return $rbac->listAdminRole();
                    break;
                case 'delete':
                    return $rbac->deleteAdminRole();
                    break;
                case 'update':
                    return $rbac->updateAdminRole();
                    break;
                case 'one':
                    return $rbac->findAdminRole();
                    break;
                case 'name':
                    return $rbac->getNameList();
                    break;
            }
        }

        return $this->fetch('editAdminRole');

    }
    ///////管理员-角色操作相关 end  ///////////////

    ///////权限操作相关 start///////////////
    public function editAccess(){
        $type = input('type');
        if($type){
            $rbac = new Rbac();
            switch ($type){
                case 'list':
                    return $rbac->listAccess();
                    break;
                case 'add':
                    return $rbac->addAccess();
                    break;
                case 'update':
                    return $rbac->updateAccess();
                    break;
                case 'delete':
                    return $rbac->deleteAccess();
                    break;
            }
        }

        return $this->fetch('editAccess');

    }
    ///////权限操作相关 end  ///////////////

    ///////权限操作相关 start///////////////
    public function editRoleAccess(){
        $type = input('type');
        if($type){
            $rbac = new Rbac();
            switch ($type){
                case 'find':
                    return $rbac->findRoleAccess();
                    break;
                case 'add':
                    return $rbac->addRoleAccess();
                    break;
                case 'update':
                    return $rbac->updateRoleAccess();
                    break;
                case 'delete':
                    return $rbac->deleteRoleAccess();
                    break;
            }
        }

        return $this->fetch('editRoleAccess');

    }
    ///////权限操作相关 end  ///////////////


    public function test(){
        $rbac = new Rbac();
        return $rbac->findAdminRole();
    }

}

