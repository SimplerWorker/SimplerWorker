<?php
namespace app\api\model;

use think\Model;
use traits\model\SoftDelete;


/**
 * Created by SimplerWorker
 * User: xiaojun.lan
 * Date: 2018/5/8
 * Time: 14:53
 */
class User extends Model
{

    #命名 tp5019_user 对应 User.php
    #   假设:tp5019_user_info 则对应 UserInfo.php文件

/*
    Create Table: CREATE TABLE `tp5019_user` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增长主键',
    `name` varchar(300) DEFAULT NULL COMMENT '用户名',
    `password` char(32) DEFAULT NULL COMMENT '密码',
    `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
    `count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '数字比较',
    `num` int(11) unsigned DEFAULT NULL COMMENT '模型中设置的值',
    `up` int(11) unsigned DEFAULT NULL COMMENT '演示行更新时自动更新为当前时间戳',
    `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
    `create_time` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
    `update_time` int(11) unsigned DEFAULT NULL COMMENT '更新时间',
    `delete_time` int(11) unsigned DEFAULT NULL COMMENT '删除时间',
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8
*/

    protected $auto = ['count']; // auto 在数据新增和修改的时候都会执行
    protected $insert = ['num']; // insert 在数据新增的时候会执行
    protected $update = ['up']; // update 在数据修改的时候会执行

    use SoftDelete;

    protected $autoWriteTimestamp = true;
    // 表中需要存在 create_time int类型的字段，
    //如果要更改名，可以设置为 protected $createTime = 'create_at'；
    //如果默认不要插入，设置为  protected $createTime = false;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time'; // 同上
    protected $deleteTime = 'delete_time'; // 同上

    /**
     * 模型中的 getSexAttr 方法名是固定的,将会判断数据库中sex字段的值,然后返回相应的替换值
     * @param $val
     * @return string
     */
    public function getSexAttr($val)
    {
        switch ($val) {
            case '0':
                return '男';
                break;
            case '1':
                return '女';
                break;
            default:
                return '未知';
                break;
        }
    }

    /**
     * 自动将用户传递进来的密码进行MD5加密，然后返回保存到数据库
     * @param $val
     * @return string
     */
    public function setPassWordAttr($val){
        return md5($val);
    }

    /**
     * 方法演示count字段在该行增加和更改时都会自动设置新的值
     * @param $val
     * @return int
     */
    public function setCountAttr($val){
        return time();
    }

    /**
     * 该方法演示num字段在插入时自动插入值
     * @param $val
     * @return int
     */
    public function setNumAttr($val){
        return time();
    }

    /**
     * 该方法演示up字段在行数据更新时，up自动更新为时间戳
     * @param $val
     * @return int
     */
    public function setUpAttr($val){
        return time();
    }

}

