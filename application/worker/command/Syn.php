<?php
/**
 * Created by SimplerWorder.
 * User: xiaojun.Lan
 * Date: 2018/6/26/026
 * Time: 22:23
 */

namespace app\worker\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use app\api\controller\Index;

class Syn extends Command
{
    protected function configure()
    {
        $this->setName('syn.sh')->setDescription('crontab syn.sh data');
    }

    protected function execute(Input $input, Output $output)
    {
        $output->writeln("start \n");


        $index = new Index();
        echo $index->syn();


        $output->writeln("\nend\n");
    }

}
