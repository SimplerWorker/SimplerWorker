<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/4
 * Time: 18:10
 */

return [
    'default_filter'         => 'htmlspecialchars,addslashes',
    'auto_bind_module'       => true, // 入口自动绑定模块
    'session'                => [
        'id'             => '',
        // SESSION_ID的提交变量,解决flash上传跨域
        'var_session_id' => '',
        // SESSION 前缀
        'prefix'         => 'think',
        // 驱动方式 支持redis memcache memcached
        'type'           => '',
        // 是否自动开启 SESSION
        'auto_start'     => true,
    ],
    'app_debug'              => true,
    'url_route_on'           => true,// 是否开启路由
    'url_route_must'         => false, // 是否强制使用路由
    'cache' => [ // 使用复合缓存类型
        'type' => 'complex',
        // 默认使用的缓存
        'default' => [
            // 驱动方式
            'type' => 'File',
            // 缓存保存目录
            'path' => @CACHE_PATH, // 使用@抑错，方便框架外部引用
        ],
        // 文件缓存
        'file' => [
            // 驱动方式
            'type' => 'file',
            // 设置不同的缓存保存目录
            'path' => @RUNTIME_PATH . 'file/', // 使用@抑错，方便框架外部引用
        ],
        // redis缓存
        'redis' => [
            // 驱动方式
            'type' => 'redis',
            // 服务器地址
            'host' => '127.0.0.1',
            //	redis端口
            'port' =>	6379,
        ],
    ],

    /******************自定义的数组字段 start *******************/
    "is_register" => true,// 是否将当前服务器配置为 register 服务器
    "register_ip_port" => '192.168.1.133:1236',// gatewayworker的webSocket注册地址

    "business_worker" => true,// 是否将当前服务器配置为 business_worker 服务器
    "business_worker_count" => 4,//  当前business_worker服务器开启的进程数
    "event_handler" => 'app\worker\controller\Events',// 服务注册地址
    "Worker_business_name" => 'WorkerBusiness',// workerBusinessName

    "gateway" => true,// 是否将当前服务器配置为 gateway 服务器
    "gateway_lan_ip" => "192.168.1.133",// 当前 gateway 服务器ip地址（内网ip）
    "gateway_web_socket" => '192.168.1.133:8082',// 服务器webSocket地址和端口，提供客户端连接，一般是当前gateway服务器外网ip和端口
    "gateway_count" => 4,// 当前gateway服务器的进程数
    "gateway_start_port" => 2300,/*当前gateway服务器的内部通讯起始端口,如果gateway_count为4，
                                    则一般会使用2300 2301 2302 2303 4个端口作为内部通讯端口*/
    "gateway_name" => 'gatewayName',//gatewayName

];


