<?php
/**
 * Created by yd
 * Windows平台下使用
 * User: xiaojun.lan
 * Date: 2018/5/15
 * Time: 9:28
 */

namespace app\worker\controller;

use Workerman\Worker;
use GatewayWorker\Register;

class Sregister
{
    public function __construct(){
        // register 服务必须是text协议
        $register = new Register('text://0.0.0.0:1236');

        // 如果不是在根目录启动，则运行runAll方法
        if(!defined('GLOBAL_START'))
        {
            Worker::runAll();
        }
    }
}
