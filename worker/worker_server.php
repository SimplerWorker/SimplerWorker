<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

/**
 * 关联文件说明
 * windows平台下由于没有守护进程概念，请进入项目目录执行：
 *  php start_register.php start_gateway.php start_businessworker.php
 * 如下文件是Windows平台下运行的：
 * start_businessworker.php   start_gateway.php  start_register.php
 * app\server\controller\Sbusinessworker.php  app\server\controller\Sgateway.php
 * app\server\controller\Sregister.php
 * 其中：app\server\view\index.html 是Windows和Linux公共文件
 *
 */

// [ 应用入口文件 ]


/**
 * 此文件只能在Linux环境运行
 * 终端进入站点根目录，并执行 php start_server.php start -d
 * 其它说明：
 *  php start_server.php status         可以查看当前进程和服务器运行状态
 *  php start_server.php connections    可以查看当前连接状态
 *  php start_server.php reload         平滑重启服务器（用户不掉线）
 *  php start_server.php restart        强制重启服务器（所有用户都会掉线）
 *  php start_server.php stop           停止服务器
 *  php start_server.php start          以调试方式启动服务器（debug状态，log将直接打印到终端）
 *  php start_server.php start -d       以守护进程方式启动服务器（产品状态，log将被写入文件）
 *
 */
ini_set('display_errors', 'on');
if(strpos(strtolower(PHP_OS), 'win') === 0)
{
    exit("start.php not support windows.\n");
}
//检查扩展
if(!extension_loaded('pcntl'))
{
    exit("Please install pcntl extension. See http://doc3.workerman.net/appendices/install-extension.html\n");
}
if(!extension_loaded('posix'))
{
    exit("Please install posix extension. See http://doc3.workerman.net/appendices/install-extension.html\n");
}

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
// 配置文件目录
define('CONF_PATH', __DIR__ . '/config/');

define("BIND_MODULE","worker/server/"); // 及时通讯server

// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';
