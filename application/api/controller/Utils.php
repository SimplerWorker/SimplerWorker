<?php
/**
 * Created by yd
 * User: xiaojun.lan
 * Date: 2018/5/29
 * Time: 14:15
 */

namespace app\api\controller;


use think\Session;

class Utils
{


    /**
     * 设置管理员session信息
     * @param $adminInfo
     */
    public static function setAdminInfo($adminInfo){

        $id = @$adminInfo['id'];
        $role_name = @$adminInfo['role_name'];
        $role_id = @$adminInfo['role_id'];
        $comment = @$adminInfo['comment'];
        $name = @$adminInfo['name'];
        $email = @$adminInfo['email'];
        $phone = @$adminInfo['phone'];
        $mobile = @$adminInfo['mobile'];
        $sex = @$adminInfo['sex'];
        $is_admin = @$adminInfo['is_admin'];
        $status = @$adminInfo['status'];
        $create_time = @$adminInfo['create_time'];
        $update_time = @$adminInfo['update_time'];
        $delete_time = @$adminInfo['delete_time'];

        Session::set('admin_id',$id);
        Session::set('admin_role_name',$role_name);
        Session::set('admin_role_id',$role_id);
        Session::set('admin_name',$name);
        Session::set('admin_comment',$comment);
        Session::set('admin_email',$email);
        Session::set('admin_phone',$phone);
        Session::set('admin_mobile',$mobile);
        Session::set('admin_sex',$sex);
        Session::set('admin_is_admin',$is_admin);
        Session::set('admin_status',$status);
        Session::set('admin_create_time',$create_time);
        Session::set('admin_update_time',$update_time);
        Session::set('admin_delete_time',$delete_time);

    }


    /**
     * 获取管理员session信息
     * @return array
     */
    public static function getAdminInfo(){

        $id = Session::get('admin_id');
        $role_name = Session::get('admin_role_name');
        $role_id = Session::get('admin_role_id');
        $name = Session::get('admin_name');
        $comment = Session::get('admin_comment');
        $email = Session::get('admin_email');
        $phone = Session::get('admin_phone');
        $mobile = Session::get('admin_mobile');
        $sex = Session::get('admin_sex');
        $is_admin = Session::get('admin_is_admin');
        $status = Session::get('admin_status');
        $create_time = Session::get('admin_create_time');
        $update_time = Session::get('admin_update_time');
        $delete_time = Session::get('admin_delete_time');

        return array(
            'id'    => $id,
            'role_name'    => $role_name,
            'role_id'    => $role_id,
            'name'  => $name,
            'comment'  => $comment,
            'email' => $email,
            'phone' => $phone,
            'mobile' => $mobile,
            'sex' => $sex,
            'is_admin' => $is_admin,
            'status' => $status,
            'create_time' => $create_time,
            'update_time' => $update_time,
            'delete_time' => $delete_time
        );

    }

    /**
     * 获取当前登录管理员的id
     * @return mixed
     */
    public static function getAdminId(){
        return Session::get('admin_id');
    }

    /**
     * 统一返回数组格式,不分页
     * @param $data array 返回的数据
     * @param $code int 返回码：1默认成功，其它为错误码，用于快速定位错误位置
     * @param string $debugMsg 调试说明
     * @return array array 拼装后的数组;debug模式（开发调试模式）下，将增加返回msg说明
     */
    public static function arrayFormat($data=array(),$code=1,$debugMsg=''){
        $inArr = false;
        $successCode = array(1,20000); // 用于保存获取正常的状态码
        if(in_array($code,$successCode)){
            $inArr = true;
        }

        $arr = array(
            'data'   => empty($data)?[]:$data, // 返回数据
            'result' => $inArr?'success':'false', // 状态
            'code'   => $code // 返回码，用于快速定位错误位置
        );

        if(config('app_debug') && !$inArr){
            $arr['msg'] = $debugMsg;
        }

        return $arr;

    }

    /**
     * 统一返回数组格式,分页
     * @param $data array 返回的数据
     * @param $totalRecord int 总记录数（表的所有行数）
     * @param $code int 返回码：1默认成功，其它为错误码，用于快速定位错误位置
     * @param string $debugMsg 调试说明
     * @return array array 拼装后的数组;debug模式（开发调试模式）下，将增加返回msg说明
     */
    public static function arrayListFormat($data=array(),$totalRecord = 0,$code=1,$debugMsg=''){
        $inArr = false;
        $successCode = array(1,20000); // 用于保存获取正常的状态码
        if(in_array($code,$successCode)){
            $inArr = true;
        }

        $arr = array(
            'data'   => empty($data)?[]:$data, // 返回数据
            'result' => $inArr?'success':'false', // 状态
            'total_record' => $totalRecord, // 总记录数（表的所有行数）
            'code'   => $code // 返回码，用于快速定位错误位置
        );

        if(config('app_debug') && !$inArr){
            $arr['msg'] = $debugMsg;
        }

        return $arr;

    }

    /**
     * 检查提交数据的地址跟登录客户端地址是否一致，不一致则可能为跨站攻击
     * @return bool
     */
    public static function isCsrf(){
        preg_match('/[\d\.]+/i',@$_SERVER['HTTP_ORIGIN'],$ip); // 获取提交数据的源ip地址
        if(@$ip[0] && @$ip[0] != $_SERVER['REMOTE_ADDR']){ // 如果获取的源ip地址存在，且跟客户端ip不一致，说明是跨站攻击，不允许请求
            return true;
        }
        return false;
    }

}

