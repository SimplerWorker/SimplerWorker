<?php
/**
 * Created by yd
 * User: xiaojun.lan
 * Date: 2018/5/31
 * Time: 19:15
 */

namespace app\api\model;


use think\Model;

class AdminRole extends Model
{

    /*
     *

    CREATE TABLE `sw_admin_role` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `admin_id` int(11) NOT NULL DEFAULT '0' COMMENT '管理员id',
        `role_id` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
        `create_time` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
        `update_time` int(11) unsigned DEFAULT NULL COMMENT '更改时间',
        PRIMARY KEY (`id`),
        KEY `idx_uid` (`admin_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员角色表'

     *
     */

    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    /**
     * 将查询到的创建时间戳转换为日期时间格式
     * @param $val
     * @return false|string
     */
    public function getCreateTimeAttr($val)
    {
        return empty($val)?'':date('Y-m-d H:i:s', $val);
    }

    /**
     * 将查询到的更新时间戳转换为日期时间格式
     * @param $val
     * @return false|string
     */
    public function getUpdateTimeAttr($val)
    {
        return empty($val)?'':date('Y-m-d H:i:s', $val);
    }


}


