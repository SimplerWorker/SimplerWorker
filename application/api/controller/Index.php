<?php
/**
 * Created by yd
 * User: xiaojun.lan
 * Date: 2018/6/7
 * Time: 9:39
 */

namespace app\api\controller;

use think\config;
use app\api\controller\Rbac;

class Index
{
    protected $isPrivilege; // 是否有权限访问

    public function _initialize()
    {
        // 设置返回数据格式，默认json格式
        $type = input('type');
        if (!in_array($type, ['json', 'xml', 'jsonp'])) {
            $type = 'json';
        }
        Config::set('default_return_type', $type);
        $rbac = new Rbac();
        $this->isPrivilege = $rbac->checkAllowVisit();

    }

    /**
     * 非超级管理员，其它界面使用方式
     * @return array
     */
    public function myPrivilege(){
        if(!$this->isPrivilege){ // 检查访问权限
            return Utils::arrayFormat([],2003,'没有访问权限');
        }
        // TODO 做一些事情
    }

    public function syn(){
        file_put_contents('./test.txt',date("Y-m-d H:i:s",time()).PHP_EOL,FILE_APPEND);
        return 'api/controller/index';
    }

}

