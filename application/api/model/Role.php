<?php
/**
 * Created by yd
 * User: xiaojun.lan
 * Date: 2018/5/31
 * Time: 19:07
 */

namespace app\api\model;


use think\Model;
use traits\model\SoftDelete;

class Role extends Model
{

    /*
     *
        CREATE TABLE `sw_role` (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `name` varchar(50) NOT NULL DEFAULT '' COMMENT '角色名称',
            `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1:有效;0:无效',
            `comment` char(50) DEFAULT NULL COMMENT '行备注',
            `create_time` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
            `update_time` int(11) unsigned DEFAULT NULL COMMENT '更新时间',
            `delete_time` int(11) unsigned DEFAULT NULL COMMENT '删除时间',
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表'
     */

    use SoftDelete;

    protected $autoWriteTimestamp = true;

    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'delete_time';

    /**
     * 将查询到的更新时间戳转换为日期时间格式
     * @param $val
     * @return false|string
     */
    public function getUpdateTimeAttr($val)
    {
        return empty($val)?'':date('Y-m-d H:i:s', $val);
    }

    /**
     * 将查询到的创建时间戳转换为日期时间格式
     * @param $val
     * @return false|string
     */
    public function getCreateTimeAttr($val)
    {
        return empty($val)?'':date('Y-m-d H:i:s', $val);
    }

    /**
     * 将查询到的软删除事件戳转为日期时间格式
     * @param $val
     * @return false|string
     */
    public function getDeleteTimeAttr($val)
    {
        return empty($val)?'':date('Y-m-d H:i:s', $val);
    }


}